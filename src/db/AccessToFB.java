package db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import static oasysreport.GlobalVariables.LOG;

public class AccessToFB {

    private static final String JDBC = "jdbc:firebirdsql://";
    private static final String JDBC_PORT = ":3050/";
    private String strConnect;
    private String strUser;
    private String strPass;

    private Connection connection = null;

    public boolean getAccess(String host, String path, String user, String pass) {
        return (setAccessParam(host, path, user, pass) && connectToDB());
    }

    private boolean setAccessParam(String host, String path, String user, String pass) {
        strConnect = JDBC + host + JDBC_PORT + path;
        strUser = user;
        strPass = pass;
        return true;
    }

    private boolean connectToDB() {
        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");
        } catch (ClassNotFoundException ex) {
            LOG.error(ex);
            return false;
        }
        try {
            Properties connInfo = new Properties();
            connInfo.put("user", strUser);
            connInfo.put("password", strPass);
            connInfo.put("charSet", "Cp1251");
            connection = java.sql.DriverManager.getConnection(strConnect, connInfo);
        } catch (SQLException ex) {
            LOG.error(ex);
            return false;
        } 
        return true;
    }

    public boolean closeAccess() {
        try {
            if (connection != null) {
                    connection.close();
                }            
        } catch (SQLException ex) {
            LOG.error(ex);
            return false;
        }
        return true;
    }

    public Connection getConnection() {
        return connection;
    }

    public Statement getStatementForFBConnect() {
        Statement statement;
        try {
            statement = connection.createStatement();
        } catch (SQLException ex) {
            LOG.error(ex);
            return null;
        }
        return statement;
    }
}
