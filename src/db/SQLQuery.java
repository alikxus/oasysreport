package db;

import java.sql.*;
import java.util.ArrayList;
import static oasysreport.GlobalVariables.LOG;

public class SQLQuery {

    private final Connection connection;
    private final Statement statement;

    public SQLQuery(Connection connection) throws SQLException {

        this.connection = connection;
        this.statement = connection.createStatement();
    }

    public ResultSet execSQLquery(ArrayList<String> sqlQuery) {

        ResultSet resultSQLquery;
        String strSQL = "";
        strSQL = sqlQuery.stream().map((iStr) -> iStr + "\n").reduce(strSQL, String::concat);
        try {
            resultSQLquery = statement != null ? statement.executeQuery(strSQL) : null;
        } catch (SQLException ex) {
            LOG.error(ex);
            return null;
        }
        return resultSQLquery;
    }

    public ResultSet execSQLquery(String strSQL) {
        ResultSet resultSQLquery;
        try {
            resultSQLquery = statement != null ? statement.executeQuery(strSQL) : null;
        } catch (SQLException ex) {
            LOG.error(ex);
            return null;
        }
        return resultSQLquery;
    }

    public String getChannelID(String fiderID, String quantID) {

        String result = "";
        String strSQL = "SELECT ID FROM CHANNELS WHERE  FIDER_ID = '" + fiderID + "' AND QUANT_ID = '" + quantID + "'";
        ResultSet resultSQLquery = execSQLquery(strSQL);
        try {
            if (resultSQLquery.next()) {
                return result = resultSQLquery.getString("ID");
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (resultSQLquery != null) {
                try {
                    resultSQLquery.close();
                } catch (SQLException ex) {
                }
            }
        }
        return result;
    }

    public String getQuantValue(String channelID, String dateInfo, String integrInt) {

        String result = "";
        String strSQL = "SELECT VAL FROM DAY_GRAPH WHERE CHANNEL_ID = '" + channelID
                + "' AND DATE_INFO = '" + dateInfo + "' AND INTEGR_INT = '" + integrInt + "'";
        ResultSet resultSQLquery = execSQLquery(strSQL);
        try {
            if (resultSQLquery.next()) {
                return result = String.valueOf(resultSQLquery.getFloat("VAL"));
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (resultSQLquery != null) {
                try {
                    resultSQLquery.close();
                } catch (SQLException ex) {
                }
            }
        }
        return result;
    }

    public String getFiderName(String fiderID) {

        String strSQL = "SELECT NAME FROM FIDERS WHERE ID = '" + fiderID + "'";
        ResultSet resultSQLquery = execSQLquery(strSQL);
        try {
            if (resultSQLquery.next()) {
                return resultSQLquery.getString("NAME");
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (resultSQLquery != null) {
                try {
                    resultSQLquery.close();
                } catch (SQLException ex) {
                }
            }
        }
        return "";
    }
    
    public String getGroupName(String groupID) {

        String strSQL = "SELECT NAME FROM GROUPS WHERE ID = '" + groupID + "'";
        ResultSet resultSQLquery = execSQLquery(strSQL);
        try {
            if (resultSQLquery.next()) {
                return resultSQLquery.getString("NAME");
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (resultSQLquery != null) {
                try {
                    resultSQLquery.close();
                } catch (SQLException ex) {
                }
            }
        }
        return "";
    }

    public String[] getMeasQuantName(String measQuantID) {

        String[] result = new String[2];
        String strSQL = "SELECT NAME, NAME_METR FROM MEAS_QUANTS WHERE ID = '" + measQuantID + "'";
        ResultSet resultSQLquery = execSQLquery(strSQL);
        try {
            if (resultSQLquery.next()) {
                result[0] = resultSQLquery.getString("NAME");
                result[1] = resultSQLquery.getString("NAME_METR");
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (resultSQLquery != null) {
                try {
                    resultSQLquery.close();
                } catch (SQLException ex) {
                }
            }
        }
        return result;
    }

    public ArrayList<String> getFiderIDsQuery(String groupID) {
        
        ArrayList<String> result = new ArrayList();
        String strSQL = "SELECT FIDER_ID FROM FIDERS_GROUPS WHERE GROUP_ID = '" + groupID + "'";
        ResultSet resultSQLquery = execSQLquery(strSQL);
        try {
            while (resultSQLquery.next()) {
                result.add(resultSQLquery.getString("FIDER_ID"));
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (resultSQLquery != null) {
                try {
                    resultSQLquery.close();
                } catch (SQLException ex) {
                }
            }
        }
        return result;
    }
}
