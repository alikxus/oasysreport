package oasysreport;

import java.util.ArrayList;
import java.util.Calendar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GlobalVariables {

    public final static Logger LOG = LogManager.getLogger(GlobalVariables.class);
    public static int indent = 6;//start 0
    public final static String[] LIST_OF_MONTHS = {"январь", "февраль", "март", "апрель",
        "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};
    public static Calendar calendar = Calendar.getInstance();
    public static String reportFileName = "";
    public static String hostDB, userDB, passwordDB, pathToDB, pathToExcelFile, pathToReportConfigFile;
    public static String interval, type, year, month, day, start, end, fiders, groups, quants, integr_int;
    public static ArrayList<String> fidersList = new ArrayList();
    public static ArrayList<String> groupsList = new ArrayList();
    public static ArrayList<String> quantsList = new ArrayList();   
    public static ArrayList<String> message = new ArrayList<>();
}
