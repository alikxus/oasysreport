package oasysreport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;
import static oasysreport.GlobalVariables.LOG;

public class CreateJsonTemplate {

    public void createJsonTemplate() {
        
        Scanner in = new Scanner(System.in);
        System.out.print("Сервер базы данных (по умолчанию - localhost): ");
        String hostFromBD = in.nextLine();
        if (hostFromBD.isEmpty()) {
            hostFromBD = "localhost";
        }
        System.out.print("Путь к базе данных: ");
        String pathFromBD = in.nextLine();
        System.out.print("Имя пользователя (по умолчанию - SYSDBA): ");
        String userFromBD = in.nextLine();
        if (userFromBD.isEmpty()) {
            userFromBD = "SYSDBA";
        }
        System.out.print("Пароль (по умолчанию - masterkey): ");
        String passFromBD = in.nextLine();
        if (passFromBD.isEmpty()) {
            passFromBD = "masterkey";
        }
        System.out.print("Путь к конфигурационному файлу отчета: ");
        String pathToReportConfigFile = in.nextLine();
        System.out.print("Путь к файлу отчета: ");
        String pathToExcelFile = in.nextLine();

        Map<String, Object> template = new HashMap<String, Object>(1);
        template.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonGeneratorFactory jgf = Json.createGeneratorFactory(template);
        try {
            File file = new File("config.json");
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            OutputStream fos = new FileOutputStream(file.getName());
            JsonGenerator jg = jgf.createGenerator(fos);
            jg.writeStartObject()
                    .write("hostBD", hostFromBD)
                    .write("pathToDB", pathFromBD)
                    .write("userBD", userFromBD)
                    .write("passBD", passFromBD)
                    .write("pathToReportConfigFile", pathToReportConfigFile)
                    .write("pathToExcelFile", pathToExcelFile)
                    .writeEnd()                    
                    .close();
        } catch (FileNotFoundException ex) {
            LOG.error(ex.getMessage());
        } catch (IOException ex) {
            LOG.error(ex.getMessage());
        }
    }
    
}
