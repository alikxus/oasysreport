package oasysreport;

import static oasysreport.GlobalVariables.LOG;

public class OasysReport {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {           
            if (InitApplication.readConfigFile()) {
                if (InitApplication.readConfigReportFile()) {
                    new CreateReport().createReport();
                } 
                else {
                    throw new UnsupportedOperationException("Иннициализация приложения не пройдена!");
                }
            } else {
                new CreateJsonTemplate().createJsonTemplate();
            }
        } catch (Exception ex) {
            LOG.error(ex);
            System.exit(0);
        }
    }
}
