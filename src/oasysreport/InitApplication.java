package oasysreport;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import static oasysreport.GlobalVariables.*;
import static oasysreport.Utils.isExistsFile;
import static oasysreport.Utils.soutMessages;

public class InitApplication {

    static boolean readConfigFile() {
        boolean result = false;
        InputStream fisJson = null;
        JsonObject jsonObject;
        try {
            if (isExistsFile("config.json")) {
                fisJson = new FileInputStream("config.json");
                try (JsonReader reader = Json.createReader(fisJson)) {
                    jsonObject = reader.readObject();
                }
                hostDB = jsonObject.getString("hostBD");
                userDB = jsonObject.getString("userBD");
                passwordDB = jsonObject.getString("passBD");
                pathToDB = jsonObject.getString("pathToDB");
                pathToExcelFile = jsonObject.getString("pathToExcelFile");
                pathToReportConfigFile = jsonObject.getString("pathToReportConfigFile");
                return true;
            } else {
                LOG.error("Не найден конфигурационный файл!");
                result = false;
            }
        } catch (FileNotFoundException ex) {
            LOG.error(ex);
            return false;
        } finally {
            if (fisJson != null) {
                try {
                    fisJson.close();
                } catch (IOException ex) {
                }
            }
            if (!message.isEmpty()) {
                soutMessages(message);
                message.clear();
            }
        }
        return result;
    }

    static boolean readConfigReportFile() {
        boolean result = false;
        InputStream fisJson = null;
        JsonObject jsonObject;
        try {
            if (isExistsFile(pathToReportConfigFile)) {
                fisJson = new FileInputStream(pathToReportConfigFile);
                try (JsonReader reader = Json.createReader(fisJson)) {
                    jsonObject = reader.readObject();
                }
                JsonObject object = jsonObject.getJsonObject("interval");
                type = object.getJsonString("type").getString();
                switch (type) {
                    case "date":
                        day = object.getJsonString("day").getString();
                        break;
                    case "month":
                        if (object.getJsonString("month").getString().length() == 1) {
                            month = "0" + object.getJsonString("month").getString();
                        } else {
                            month = object.getJsonString("month").getString();
                        }
                        year = object.getJsonString("year").getString();
                        break;
                    case "interval":
                        start = object.getJsonString("start").getString();
                        end = object.getJsonString("end").getString();
                        break;
                    default:
                        throw new UnsupportedOperationException("Не верный интервал!");
                }
                object = jsonObject.getJsonObject("object");
                JsonArray fiders = object.getJsonArray("fiders");
                fiders.forEach((fider) -> {
                    fidersList.add(fider.toString().replaceAll("\"", ""));
                });
                JsonArray groups = object.getJsonArray("groups");
                groups.forEach((group) -> {
                    groupsList.add(group.toString().replaceAll("\"", ""));
                });
                JsonArray quants = jsonObject.getJsonArray("quants");
                quants.forEach((quant) -> {
                    quantsList.add(quant.toString().replaceAll("\"", ""));
                });
                integr_int = jsonObject.getString("integr_int");
                return true;
            } else {
                LOG.error("Не найден конфигурационный файл отчета!");
                result = false;
            }
        } catch (FileNotFoundException ex) {
            LOG.error(ex);
            return false;
        } finally {
            if (fisJson != null) {
                try {
                    fisJson.close();
                } catch (IOException ex) {
                }
            }
            if (!message.isEmpty()) {
                soutMessages(message);
                message.clear();
            }
        }
        return result;
    }
}
