package oasysreport;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;
import static oasysreport.GlobalVariables.LOG;
import static oasysreport.GlobalVariables.end;
import static oasysreport.GlobalVariables.month;
import static oasysreport.GlobalVariables.start;
import static oasysreport.GlobalVariables.type;
import static oasysreport.GlobalVariables.year;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;

public class Utils {

    private static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    public static boolean isExistsFile(String path) {
        return new File(path).exists();
    }

    public static void soutMessages(ArrayList<String> message) {
        for (int j = 0; j < message.size(); j++) {
            System.out.println(message.get(j));
            LOG.info(message.get(j));
        }
    }

    public static String getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return new SimpleDateFormat("YYYY").format(calendar.getTime());
    }

    public static String getDay(String dateStr) {

        String sdf = "";
        if (dateStr != null && !dateStr.isEmpty()) {
            try {
                Date date = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.S").parse(dateStr);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                sdf = new SimpleDateFormat("dd").format(calendar.getTime());
            } catch (ParseException ex) {
                LOG.error(ex);
            }
        }
        return sdf;
    }

    public static String getNextDay(String dateStr) {

        String sdf = "";
        try {
            Date date = new SimpleDateFormat("dd.MM.yyyy").parse(dateStr);
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(date);
            if (dateStr != null && !dateStr.isEmpty()) {
                switch (type) {
                    case "date":
                        calendar.add(Calendar.DAY_OF_MONTH, 1);
                        sdf = new SimpleDateFormat("dd.MM.yyyy").format(calendar.getTime());
                    case "month":

                    case "interval":
                        break;
                    default:
                        throw new UnsupportedOperationException("Не верный интервал!");
                }
            }
            return sdf;
        } catch (ParseException ex) {
            LOG.error(ex);
        }
        return sdf;
    }

    public static String getDateNow() {
        Calendar calendar = Calendar.getInstance();
        return new SimpleDateFormat("dd.MM.yyyy").format(calendar.getTime());
    }

    public static int getCountDay(String type) {

        int countDay = 0;
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        try {
            switch (type) {
                case "date":
                    countDay = 1;
                    break;
                case "month":
                    calendar1.set(Calendar.YEAR, Integer.parseInt(year));
                    calendar1.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
                    calendar1.set(Calendar.DAY_OF_MONTH, calendar1.getActualMaximum(Calendar.DAY_OF_MONTH));
                    countDay = calendar1.getActualMaximum(Calendar.DAY_OF_MONTH);
                    break;
                case "interval":
                    Date date;
                    date = sdf.parse(start);
                    calendar1.setTime(date);
                    date = sdf.parse(end);
                    calendar2.setTime(date);
                    countDay = daysBetween(calendar1.getTime(), calendar2.getTime()) + 1;
                    break;
                default:
                    throw new UnsupportedOperationException("Не верный интервал!");
            }
        } catch (ParseException ex) {
            LOG.error(ex);
        }
        return countDay;
    }

    public static int daysBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    public static void setBoldToCell(HSSFWorkbook workbook, Cell cell) {

        CellStyle cellStyle = cell.getCellStyle();
        HSSFFont font = (HSSFFont) workbook.createFont();
        font.setBold(true);
        cellStyle.setFont(font);
        cell.setCellStyle(cellStyle);
    }
}
