package oasysreport;

import db.AccessToFB;
import db.SQLQuery;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import static oasysreport.GlobalVariables.*;
import static oasysreport.Utils.getDateNow;
import static oasysreport.Utils.getCountDay;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;

public class CreateReport {

    static int startTitleColumn = 1;

    public void createReport() throws ParseException {
        if (reportFileName.isEmpty()) {
            switch (type) {
                case "date":
                    reportFileName = getDateNow() + "_" + type + "_" + day;
                    break;
                case "month":
                    reportFileName = getDateNow() + "_" + type + "_" + month + "." + year;
                    break;
                case "interval":
                    reportFileName = getDateNow() + "_" + type + "_" + start + "_" + end;
                    break;
                default:
                    throw new UnsupportedOperationException("Не верный интервал!");
            }
            if (createReportfile(reportFileName)) {
                System.out.println("success");
            }

        }
    }

    private boolean createReportfile(String reportFileName) throws ParseException {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Отчет");
        HSSFCell cell;

        switch (type) {
            case "date":
                createRowAndCell(getCountDay(type), sheet);
                break;
            case "month":
                createRowAndCell(getCountDay(type), sheet);
                break;
            case "interval":
                createRowAndCell(getCountDay(type), sheet);
                break;
            default:
                throw new UnsupportedOperationException("Не верный интервал!");
        }
        cell = sheet.getRow(1).getCell(1);
        createTitleReport(cell);
        createTimeColumn(sheet);
        if (!fidersList.isEmpty()) {
            createFidersReport(workbook);
            if (groupsList.isEmpty()) {
                createTotalColumn(workbook);
            }
        }
        if (!groupsList.isEmpty()) {
            createGroupsReport(workbook);
            createTotalColumn(workbook);
        }
        sheet.autoSizeColumn(0);
        try (FileOutputStream out = new FileOutputStream(new File(pathToExcelFile
                + "\\" + reportFileName + ".xls"))) {
            workbook.write(out);
            return true;
        } catch (IOException ex) {
            LOG.error(ex);
        }
        return false;
    }

    private void createRowAndCell(int countDay, HSSFSheet sheet) {

        Row row;
        int countRow = 7 + countDay * 24 * 60 / Integer.parseInt(integr_int) + 2 + 2;
        for (int i = 0; i < countRow; i++) {
            row = sheet.createRow(i);
            for (int j = 0; j < 256; j++) {
                row.createCell(j);
            }
        }
    }

    private void createTimeColumn(HSSFSheet sheet) {

        int integrInt = Integer.parseInt(integr_int);
        int countCell;
        Date date;
        Calendar calendar;

        try {
            sheet.getRow(7).getCell(0).setCellValue("Время");
            switch (type) {
                case "date":
                    date = new SimpleDateFormat("dd.MM.yyyy").parse(day);
                    calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.MINUTE, integrInt);
                    countCell = 24 * 60 / integrInt;
                    for (int i = 0; i < countCell; i++) {
                        sheet.getRow(i + indent + 3).getCell(0).setCellValue(
                                new SimpleDateFormat("dd.MM.yyyy HH:mm").format(calendar.getTime()));
                        calendar.add(Calendar.MINUTE, integrInt);
                    }
                    sheet.getRow(countCell + indent + 3).getCell(0).setCellValue("Всего");
                    break;
                case "month":
                    date = new SimpleDateFormat("dd.MM.yyyy").parse("01." + month + "." + year);
                    calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.MINUTE, integrInt);
                    countCell = 24 * 60 * getCountDay(type) / integrInt;
                    for (int i = 0; i < countCell; i++) {
                        sheet.getRow(i + indent + 3).getCell(0).setCellValue(
                                new SimpleDateFormat("dd.MM.yyyy HH:mm").format(calendar.getTime()));
                        calendar.add(Calendar.MINUTE, integrInt);
                    }
                    sheet.getRow(countCell + indent + 3).getCell(0).setCellValue("Всего");
                    break;
                case "interval":
                    date = new SimpleDateFormat("dd.MM.yyyy").parse(start);
                    calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.MINUTE, integrInt);
                    countCell = 24 * 60 * getCountDay(type) / integrInt;
                    for (int i = 0; i < countCell; i++) {
                        sheet.getRow(i + indent + 3).getCell(0).setCellValue(
                                new SimpleDateFormat("dd.MM.yyyy HH:mm").format(calendar.getTime()));
                        calendar.add(Calendar.MINUTE, integrInt);
                    }
                    sheet.getRow(countCell + indent + 3).getCell(0).setCellValue("Всего");
                    break;
                default:
                    throw new UnsupportedOperationException("Не верный интервал!");
            }
        } catch (ParseException ex) {
            LOG.error(ex);
        }

    }

    private void createTitleReport(HSSFCell cell) {
        switch (type) {
            case "date":
                cell.setCellValue("Отчет за " + day);
                break;
            case "month":
                cell.setCellValue("Отчет за " + month + "." + year);
                break;
            case "interval":
                cell.setCellValue("Отчет за " + start + "-" + end);
                break;
            default:
                throw new UnsupportedOperationException("Не верный интервал!");
        }
    }

    private void createFidersReport(HSSFWorkbook workbook) {

        CellStyle style = workbook.createCellStyle();
        HSSFSheet sheet = workbook.getSheet("Отчет");
        DataFormat format = workbook.createDataFormat();
        int columnTitleCount;

        AccessToFB access = new AccessToFB();

        if (access.getAccess(hostDB, pathToDB, userDB, passwordDB)) {
            try {
                columnTitleCount = quantsList.size();
                if (columnTitleCount > 0) {
                    SQLQuery sqlQuery = new SQLQuery(access.getConnection());
                    for (int i = 0; i < fidersList.size(); i++) {
                        String titleFider = sqlQuery.getFiderName(fidersList.get(i));
                        if (!titleFider.isEmpty()) {
                            sheet.getRow(indent).getCell(startTitleColumn).setCellValue(titleFider);
                        }
                        for (int j = 0; j < quantsList.size(); j++) {
                            String[] titleMeasQuant = sqlQuery.getMeasQuantName(quantsList.get(j));
                            sheet.getRow(indent + 1).getCell(startTitleColumn + j).setCellValue(titleMeasQuant[0]);
                            sheet.getRow(indent + 2).getCell(startTitleColumn + j).setCellValue(titleMeasQuant[1]);
                            Iterator<Row> rowIterator = sheet.iterator();
                            while (rowIterator.hasNext()) {
                                Row row = rowIterator.next();
                                if (row.getRowNum() > indent + 2) {
                                    if (sheet.getRow(row.getRowNum()).getCell(0).getStringCellValue().equals("Всего")) {
                                        style.setDataFormat(format.getFormat("#,##0.000"));
                                        sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                        sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(
                                                "SUM(" + sheet.getRow(indent + 3).getCell(startTitleColumn + j).getAddress()
                                                + ":" + sheet.getRow(row.getRowNum() - 1).getCell(startTitleColumn + j).getAddress() + ")");
                                        break;
                                    }
                                    String channelID = sqlQuery.getChannelID(fidersList.get(i),
                                            quantsList.get(j));
                                    if (!channelID.isEmpty()) {
                                        String value = sqlQuery.getQuantValue(channelID,
                                                sheet.getRow(row.getRowNum()).getCell(0).getStringCellValue(),
                                                integr_int);
                                        if (!value.isEmpty()) {
                                            style.setDataFormat(format.getFormat("#,##0.000"));
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellValue(Float.parseFloat(value));
                                        }
                                    }
                                }
                            }
                        }
                        startTitleColumn = startTitleColumn + columnTitleCount;
                    }
                }
            } catch (SQLException ex) {
                LOG.error(ex);
            } finally {
                access.closeAccess();
            }
        }
    }

    private void createGroupsReport(HSSFWorkbook workbook) {

        CellStyle style = workbook.createCellStyle();
        HSSFSheet sheet = workbook.getSheet("Отчет");
        DataFormat format = workbook.createDataFormat();
        int columnTitleCount;

        AccessToFB access = new AccessToFB();
        if (access.getAccess(hostDB, pathToDB, userDB, passwordDB)) {
            try {
                columnTitleCount = quantsList.size();
                if (columnTitleCount > 0) {
                    SQLQuery sqlQuery = new SQLQuery(access.getConnection());
                    for (int i = 0; i < groupsList.size(); i++) {
                        String titleGroup = sqlQuery.getGroupName(groupsList.get(i));
                        if (!titleGroup.isEmpty()) {
                            sheet.getRow(indent).getCell(startTitleColumn).setCellValue(titleGroup);
                        }
                        for (int j = 0; j < quantsList.size(); j++) {
                            String[] titleMeasQuant = sqlQuery.getMeasQuantName(quantsList.get(j));
                            sheet.getRow(indent + 1).getCell(startTitleColumn + j).setCellValue(titleMeasQuant[0]);
                            sheet.getRow(indent + 2).getCell(startTitleColumn + j).setCellValue(titleMeasQuant[1]);
                            Iterator<Row> rowIterator = sheet.iterator();
                            while (rowIterator.hasNext()) {
                                Row row = rowIterator.next();
                                if (row.getRowNum() > indent + 2) {
                                    if (sheet.getRow(row.getRowNum()).getCell(0).getStringCellValue().equals("Всего")) {
                                        style.setDataFormat(format.getFormat("#,##0.000"));
                                        sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                        sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(
                                                "SUM(" + sheet.getRow(indent + 3).getCell(startTitleColumn + j).getAddress()
                                                + ":" + sheet.getRow(row.getRowNum() - 1).getCell(startTitleColumn + j).getAddress() + ")");
                                        break;
                                    }
                                    String channelID = sqlQuery.getChannelID(groupsList.get(i),
                                            quantsList.get(j));
                                    if (!channelID.isEmpty()) {
                                        ArrayList<String> fiderIDs = sqlQuery.getFiderIDsQuery(channelID);
                                        float sumVal = 0;
                                        boolean iter = false;
                                        for (String fiderID : fiderIDs) {
//                                            System.out.println(fiderID);                                            
                                            String value = sqlQuery.getQuantValue(channelID,
                                                    sheet.getRow(row.getRowNum()).getCell(0).getStringCellValue(),
                                                    integr_int);
                                            if (!value.isEmpty()) {
                                                sumVal = sumVal + Float.parseFloat(value);
                                                iter = true;
                                            }
                                        }
                                        style.setDataFormat(format.getFormat("#,##0.000"));
                                        sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                        if (iter) {
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellValue(sumVal);
                                        }
                                    }
                                }
                            }
                        }
                        startTitleColumn = startTitleColumn + columnTitleCount;
                    }
                }
            } catch (SQLException ex) {
                LOG.error(ex);
            } finally {
                access.closeAccess();
            }
        }
    }

    private void createTotalColumn(HSSFWorkbook workbook) {

        CellStyle style = workbook.createCellStyle();
        HSSFSheet sheet = workbook.getSheet("Отчет");
        DataFormat format = workbook.createDataFormat();
        int columnTitleCount;
        StringBuilder amount;
        style.setDataFormat(format.getFormat("#,##0.000"));

        AccessToFB access = new AccessToFB();
        if (access.getAccess(hostDB, pathToDB, userDB, passwordDB)) {
            try {
                columnTitleCount = quantsList.size();
                if (columnTitleCount > 0) {
                    SQLQuery sqlQuery = new SQLQuery(access.getConnection());
                    for (int i = 0; i < groupsList.size(); i++) {
                        String titleGroup = sqlQuery.getGroupName(groupsList.get(i));
                        if (!titleGroup.isEmpty()) {
                            sheet.getRow(indent).getCell(startTitleColumn).setCellValue("Общий расход");
                        }
                        for (int j = 0; j < quantsList.size(); j++) {
                            String[] titleMeasQuant = sqlQuery.getMeasQuantName(quantsList.get(j));
                            sheet.getRow(indent + 1).getCell(startTitleColumn + j).setCellValue(titleMeasQuant[0]);
                            sheet.getRow(indent + 2).getCell(startTitleColumn + j).setCellValue(titleMeasQuant[1]);
                            Iterator<Row> rowIterator = sheet.iterator();
                            while (rowIterator.hasNext()) {
                                Row row = rowIterator.next();
                                if (row.getRowNum() > indent + 2) {
                                    if (sheet.getRow(row.getRowNum()).getCell(0).getStringCellValue().equals("Всего")) {
                                        sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                        sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(
                                                "SUM(" + sheet.getRow(indent + 3).getCell(startTitleColumn + j).getAddress()
                                                + ":" + sheet.getRow(row.getRowNum() - 1).getCell(startTitleColumn + j).getAddress() + ")");
                                        break;
                                    }
                                    int countCol = (fidersList.size() + groupsList.size()) * quantsList.size();
                                    int count = 0;
                                    switch (quantsList.size()) {
                                        case 1:
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol; k++) {
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - k - 1).getAddress());
                                                amount.append(",");
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            System.out.println(amount);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        case 2:                                            
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol / quantsList.size(); k++) {//                                                
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - quantsList.size() - count).getAddress());
                                                amount.append(",");
                                                count = count + quantsList.size();
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        case 3:
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol / quantsList.size(); k++) {//                                                
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - quantsList.size() - count).getAddress());
                                                amount.append(",");
                                                count = count + quantsList.size();
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        case 4:
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol / quantsList.size(); k++) {//                                                
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - quantsList.size() - count).getAddress());
                                                amount.append(",");
                                                count = count + quantsList.size();
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        case 5:
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol / quantsList.size(); k++) {//                                                
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - quantsList.size() - count).getAddress());
                                                amount.append(",");
                                                count = count + quantsList.size();
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        case 6:
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol / quantsList.size(); k++) {//                                                
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - quantsList.size() - count).getAddress());
                                                amount.append(",");
                                                count = count + quantsList.size();
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        case 7:
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol / quantsList.size(); k++) {//                                                
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - quantsList.size() - count).getAddress());
                                                amount.append(",");
                                                count = count + quantsList.size();
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        case 8:
                                            amount = new StringBuilder();
                                            amount.append("SUM(");
                                            for (int k = 0; k < countCol / quantsList.size(); k++) {//                                                
                                                amount.append(sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j - quantsList.size() - count).getAddress());
                                                amount.append(",");
                                                count = count + quantsList.size();
                                            }
                                            amount.deleteCharAt(amount.length() - 1);
                                            amount.append(")");
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellStyle(style);
                                            sheet.getRow(row.getRowNum()).getCell(startTitleColumn + j).setCellFormula(amount.toString());
                                            break;
                                        default:
                                            throw new UnsupportedOperationException("Не верный MEAS_QUANTS!");
                                    }
                                }
                            }
                        }
                        startTitleColumn = startTitleColumn + columnTitleCount;
                    }
                }
            } catch (SQLException ex) {
                LOG.error(ex);
            } finally {
                access.closeAccess();
            }
        }
    }
}
